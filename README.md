# README #
This repository is a collection of functions which allows user to pinpoint the location of the requested IP Addresses via MaxMind API and extract relative data into Keboola Connection. 
It is *mandatory* for the user to own or purchase a MaxMind account for the use of this application.

### Configuration ###

Application will output an empty row if requested IP address is not valid or not found.

Required Information:

  1. MaxMind User ID
  
        - To obtain User ID:
            Account Information -> My License Key -> User ID
            
      **Note: This is not the MaxMind Account login information.**

  2. MaxMind License Key
  
        - To obtain key:
            Account Information -> My License Key -> License key

  3. IP Column Name
  
        - Source table column which contains all the IP addresses the user wants to locate
        
      **Note: Program is case-sensitive. Please enter the column name exactly as shown in the file.**


### Full Documentation ###

For full MaxMind API descriptions   
Please visit the link [here](https://dev.maxmind.com/).     


## MaxMind Terms of Use
[Official website](https://www.maxmind.com/en/geoip-site-license-overview)


### Contact Info ###

Leo Chan  
Vancouver, Canada (PST time)  
Email: leo@keboola.com  
Private: cleojanten@hotmail.com  
