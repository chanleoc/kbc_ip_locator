# Terms and Conditions

The IP Address locator (MaxMind GeoIP) is a custom science application built and offered by Leo as a third party component. It is provided as-is, without guarantees and support, and for no additional charge. 
Component's task is to help user to determine the location of the requested IP address. 
API call is process by using user-entered keys for API authentication, no sensitive information is being sent non-standard way, maintaining all Keboola receommended security standards along the way.

## MaxMind Terms of Use
[Official website](https://www.maxmind.com/en/geoip-site-license-overview)

## Contact

Leo Chan   
Vancouver, Canada (PST time)   
Email: leo@keboola.com   
Private: cleojanten@hotmail.com   