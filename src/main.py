"__author__ = 'Leo Chan'"
"__credits__ = 'Keboola 2018'"

"""
IP Address Locator
Python 3 environment 
"""

#import pip
#pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'geoip2'])
#pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'pygelf'])

import sys
import os
import logging
import pandas as pd
from keboola import docker
import logging_gelf.formatters
import logging_gelf.handlers
from geoip2.webservice import Client
from geoip2.errors import AddressNotFoundError, AuthenticationError, GeoIP2Error,HTTPError, InvalidRequestError, OutOfQueriesError,PermissionRequiredError


# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

# Setup logger
logger = logging.getLogger()
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S"
    )

logger = logging.getLogger()
logging_gelf_handler = logging_gelf.handlers.GELFTCPSocketHandler(
    host=os.getenv('KBC_LOGGER_ADDR'),
    port=int(os.getenv('KBC_LOGGER_PORT'))
    )
logging_gelf_handler.setFormatter(logging_gelf.formatters.GELFFormatter(null_character=True))
logger.addHandler(logging_gelf_handler)

# removes the initial stdout logging
logger.removeHandler(logger.handlers[0])


# initialize application
cfg = docker.Config('/data/')

# access the supplied parameters
params = cfg.get_parameters()
USER_ID = cfg.get_parameters()["user_id"]
LICENSE_KEY = cfg.get_parameters()["#license_key"]
ip_column = cfg.get_parameters()["ip_column"]

# file path
output_file_name = "ip_locator.csv"
DEFAULT_INPUT_FILE = "/data/in/tables/"
DEFAULT_OUTPUT_FILE = "/data/out/tables/"
output_file = DEFAULT_OUTPUT_FILE+output_file_name

# Get proper list of tables
cfg = docker.Config('/data/')
in_tables = cfg.get_input_tables()
logging.info("IN tables mapped: "+str(in_tables))


def get_tables(in_tables):
    """
    INPUT AND OUTPUT MAPPING
    Evaluate input and output table names.
    Only taking the first one into consideration!
    """

    table = in_tables[0]
    in_name = table["full_path"]
    in_destination = table["destination"]
    logging.info("Data table: " + str(in_name))
    logging.info("Input table source: " + str(in_destination))

    return in_name


def get_data(response,alist):
    """
    Appending IP_address information into the list for output
    """
    alist.append(response.location.longitude)
    alist.append(response.location.latitude)
    alist.append(response.postal.code)
    alist.append(response.city.name)
    alist.append(response.subdivisions.most_specific.name)
    alist.append(response.subdivisions.most_specific.iso_code)
    alist.append(response.country.name)
    alist.append(response.country.iso_code)
    alist.append(response.continent.name)
    alist.append(response.continent.code)

    return alist


if __name__ == "__main__":
    """
    Main execution script.
    """
    # Column Headers setup
    column_headers = [ip_column]
    new_headers = [
        "longitude","latitude","postal_code","city","subdivision","subdivision_code",
        "country","country_code","continent","continent_code"
    ]
    for i in new_headers:
        column_headers.append(i)
    
    # input file fetch
    in_file = get_tables(in_tables)
    data = pd.read_csv(in_file, dtype=str)
    try:
        data[ip_column]
        logging.info("IP Column verified.")
    except Exception:
        logging.error("ERROR: IP Column({0}) does not exist in source file.".format(ip_column))
        logging.error("Exit.")
        sys.exit(1)

    # API connection
    client = Client(USER_ID, LICENSE_KEY)

    # output list
    full_list = []

    """API request per ip_address"""
    for i in data[ip_column]:
        one_row_data = [i]
        try:
            response = client.insights(i)
            logging.info("Processing: {0}".format(i))
            one_row_data = get_data(response, one_row_data)
        # Skip line if requested IP address is not valid
        except (ValueError,InvalidRequestError,AddressNotFoundError) as error:
            logging.warning("WARNING: {0}".format(error))
            for j in column_headers[1:]:
                one_row_data.append("")
        # Exit code if user is not authorized/out of queries/perimission
        except (AuthenticationError,OutOfQueriesError,PermissionRequiredError) as error:
            logging.error("ERROR: {0}".format(error))
            logging.error("Exit.")
            sys.exit(1)
        
        full_list.append(one_row_data)

    """ Output results """
    results = pd.DataFrame(full_list, columns=column_headers)
    results.to_csv(output_file, index=False)


logging.info("Done.")